#include <iostream>
#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>

using namespace std;

struct node { int	first,second,pos,l,r,fa; }tree[51000];

int	n;
bool	cmp1(const node & temp_1,const node & temp_2) { return temp_1.first<temp_2.first; }
bool	cmp2(const node & temp_1,const node & temp_2) { return temp_1.pos<temp_2.pos; }

void	Insert(const int & i)
{
	int	j=i-1;

	while(tree[j].second>tree[i].second)
		j=tree[j].fa;

	tree[i].l=tree[j].r;
	tree[tree[j].r].fa=i;
	tree[j].r=i;
	tree[i].fa=j;
	return ;
}

int main()
{
	int	i;

	scanf("%d",&n);

	tree[0].l=tree[0].r=tree[0].fa=0;
	tree[0].second=-0x3f3f3f3f;

	for(i=1;i<=n;++i)
	{
		scanf("%d%d",&tree[i].first,&tree[i].second);
		tree[i].pos=i;
	}

	sort(tree+1,tree+n+1,cmp1);

	for(i=1;i<=n;++i)
	{
		tree[i].l=tree[i].r=tree[i].fa=0;
		Insert(i);
	}

	for(i=1;i<=n;++i)
	{
		tree[i].l=tree[tree[i].l].pos;
		tree[i].r=tree[tree[i].r].pos;
		tree[i].fa=tree[tree[i].fa].pos;
	}	

	sort(tree+1,tree+n+1,cmp2);

	printf("YES\n");

	for(i=1;i<=n;++i)
	{
		printf("%d %d %d\n",tree[i].fa,tree[i].l,tree[i].r);
	}

	return 0;
}
