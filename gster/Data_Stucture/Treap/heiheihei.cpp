#include<cstdio>
#include<algorithm>
using namespace std;
typedef pair<int,int>pii;
typedef long long ll;
#define mp make_pair
#define fir first
#define sec second
#define maxn 100005
int n,m,root,sta[maxn];
struct node{ 
	int rnd,l,r,sz; 
	ll v,sum,cvr;
}tr[maxn];
void push_up(int k){
	tr[k].sum=tr[tr[k].l].sum+tr[k].v+tr[tr[k].r].sum;
	tr[k].sz=tr[tr[k].l].sz+1+tr[tr[k].r].sz;
}
void push_now(int k,ll x){ 
	tr[k].v+=x,tr[k].sum+=x*tr[k].sz,tr[k].cvr+=x; 
}
void push_down(int k){
	if(tr[k].cvr){
		push_now(tr[k].l,tr[k].cvr);
		push_now(tr[k].r,tr[k].cvr);
		tr[k].cvr=0;
	}
}
void ddd(int k){
	if(!k)return;
	ddd(tr[k].l),ddd(tr[k].r);
	push_up(k);
}
void build(){
	int top=0;
	for(int i=1;i<=n;i++){
		bool flag=false;
		while(top&&tr[i].rnd>tr[sta[top]].rnd){
			flag=true;
			tr[sta[top-1]].r=sta[top],top--;
		}
		if(flag)tr[i].l=sta[top+1];
		sta[++top]=i;
	}
	while(top)tr[sta[top-1]].r=sta[top],top--;
	ddd(root=sta[1]);
}
int merge(int x,int y){//x中最大元素小于y中最小元素
	if(x*y==0)return x+y;
	if(tr[x].rnd>=tr[y].rnd){
		push_down(x);
		tr[x].r=merge(tr[x].r,y),push_up(x);
		return x;
	}
	else{
		push_down(y);
		tr[y].l=merge(x,tr[y].l),push_up(y);
		return y;
	}
}
pii split(int k,int x){
	if(!k)return mp(0,0);
	if(!x)return mp(0,k);
	push_down(k);
	if(x<=tr[tr[k].l].sz){
		pii haha=split(tr[k].l,x);
		tr[k].l=haha.sec,push_up(k);
		return mp(haha.fir,k);
	}
	else{
		pii haha=split(tr[k].r,x-tr[tr[k].l].sz-1);
		tr[k].r=haha.fir,push_up(k);
		return mp(k,haha.sec);
	}

}
int main(){
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++){
		scanf("%lld",&tr[i].v);
		tr[i].rnd=rand(),tr[i].sz=1;
	}
	build();
	int a,b; ll c;
	char op[5];
	for(int i=1;i<=m;i++){
		scanf("%s",op);
		if(op[0]=='C'){
			scanf("%d%d%lld",&a,&b,&c);
			pii haha=split(root,b),hehe=split(haha.fir,a-1);
			push_now(hehe.sec,c);
			root=merge(merge(hehe.fir,hehe.sec),haha.sec);
		}
		else{
			scanf("%d%d",&a,&b);
			pii haha=split(root,b),hehe=split(haha.fir,a-1);
			printf("%lld\n",tr[hehe.sec].sum);
			root=merge(merge(hehe.fir,hehe.sec),haha.sec);
		}
	}
	return 0;
}

