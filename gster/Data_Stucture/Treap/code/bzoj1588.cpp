#include <bits/stdc++.h>

using namespace std;

struct Treap
{
	struct node
	{
		int	key,val,size,cnt;
		node	*l,*r;
		node() { key=val=size=cnt=0; l=r=0; }
	}U[110000],*root;

	int	top,ALL;
	node	*Trash[110000];
	
	Treap()
	{
		top=0,ALL=0;
		root=ALLOC();
		root->val=0x80808080;
		root->key=rand();
		root->size=2;
		root->cnt=1;
		root->r=ALLOC();
		root->r->key=rand();
		root->r->val=0x7f7f7f7f;
		root->r->size=1;
		root->r->cnt=1;
		if(root->r->key<root->key)lturn(root);
		return ;
	}

	node*	ALLOC() { return top ? Trash[top--]:U+(++ALL); }
	void	RECYCLE(node *t) { Trash[++top]=t;memset(t,0,sizeof(node)); return ; }

	void	update(node *t)
	{
		t->size=t->cnt+(t->l?t->l->size:0)+
			(t->r?t->r->size:0);
	}

	void	lturn(node *& t)
	{
		node * temp=t;
		t=t->r;
		temp->r=t->l;
		t->l=temp;
		update(t->l);
		update(t);
	}
	void	rturn(node *& t)
	{
		node * temp=t;
		t=t->l;
		temp->l=t->r;
		t->r=temp;
		update(t->r);
		update(t);
	}

	void	Insert(node *&t,const int d)
	{
		if(!t)
		{
			t=ALLOC();
			t->key=rand();
			t->val=d;
			t->size=1;
			t->cnt=1;
			return ;
		}
		t->size++;
		if(t->val==d)t->cnt++;
		else if(d>t->val)
		{
			Insert(t->r,d);
			if(t->r->key<t->key)lturn(t);
		}
		else
		{
			Insert(t->l,d);
			if(t->l->key<t->key)rturn(t);
		}
		return ;
	}

	void	Get_pre(node *t,const int d,node *& Ans)
	{
		if(!t)return ;
		if(t->val==d){Ans=t;return ;}
		if(d>t->val){Ans=t;return Get_pre(t->r,d,Ans);}
		return Get_pre(t->l,d,Ans);
	}
	void	Get_nxt(node *t,const int d,node *& Ans)
	{
		if(!t)return ;
		if(t->val==d){Ans=t;return ;}
		if(d<t->val){Ans=t;return Get_nxt(t->l,d,Ans);}
		return Get_nxt(t->r,d,Ans);
	}

	void	insert(const int d)
	{
		Insert(root,d);
	}
	int	get_pre(const int x)
	{
		node * temp;
		Get_pre(root,x,temp);
		return temp->val;
	}
	int	get_nxt(const int x)
	{
		node * temp;
		Get_nxt(root,x,temp);
		return temp->val;
	}
}S;

int	Abs(const int x) { return x>0 ? x:-x; }

int main()
{
	int	n,i,x,Ans=0;
	scanf("%d",&n);
	for(i=1;i<=n;++i)
	{
		if(scanf("%d",&x)==EOF)x=0;
		if(i!=1)Ans+=min(Abs(S.get_pre(x)-x),Abs(S.get_nxt(x)-x));
		else Ans+=x;
		S.insert(x);
	}
	printf("%d\n",Ans);
	return 0;
}
