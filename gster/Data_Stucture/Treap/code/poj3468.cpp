#include <bits/stdc++.h>

using namespace std;

struct Treap
{
	struct node
	{
		int	key,val,size,f,Sum;
		node	*l,*r,*fa;
		node() { key=rand(); val=f=Sum=0; size=1; }
	}*root,U[110000];

	int	ALL;

	Treap() { ALL=0; root=ALLOC(); }
	node*	ALLOC() { return U+(++ALL); }

	node * Merge(node *& t1,node *& t2)
	{
		if(!t1)return t2;if(!t2)return t1;
		push_down(t1),push_down(t2);
		if(t1->key < t2->key) return t1->r=Merge(t1->r,t2),update(t1),t1;
		return t2->l=Merge(t1,t2->l),update(t2),t2;
	}

	void	push_down(node * t)
	{
		if(!t || !t->f)return ;
		t->val+=t->f;
		t->Sum+=t->size*t->f;
		if(t->l)t->l->f+=t->f;
		if(t->r)t->r->f+=t->f;
		t->f=0;
	}

	void	update(node * t)
	{
		push_down(t->l);
		push_down(t->r);
		t->size=(t->l?t->l->size:0)+
			(t->r?t->r->size:0)+1;
		t->Sum=(t->l?t->l->Sum:0)+(t->r?t->r->Sum:0)+t->val;
	}


	pair<node*,node*> split(node *& t,const int pos)
	{
		if(!t)	return make_pair((node*)0,(node*)0);
		if(!pos)return make_pair((node*)0,t);
		push_down(t);
		if(t->l && pos==t->l->size)
		{
			node	*temp=t->l;
			return t->l=0,update(t),make_pair(temp,t);
		}
		if(t->l && pos==t->l->size+1)
		{
			node	*temp=t->r;
			return t->r=0,update(t),make_pair(t,temp);
		}
		if(t->l && pos<t->l->size)
		{
			pair<node*,node*>temp=split(t->l,pos);
			return t->l=temp.second,update(t),make_pair(temp.first,t);
		}
		if(!t->r)return make_pair(t,(node*)0);
		pair<node*,node*>temp=split(t->r,pos-t->r->size-1);
		return t->r=temp.first,update(t),make_pair(t,temp.second);
	}

	void	insert(const int pos,const int d)
	{
		pair<node*,node*>temp=split(root,pos);
		node	*nd=ALLOC();
		nd->val=d;
		temp.first=Merge(temp.first,nd);
		update(temp.first);
		root=Merge(temp.first,temp.second);
		update(root);
	}

	void	add(const int l,const int r,const int d)
	{
		pair<node*,node*>temp1=split(root,l);
		pair<node*,node*>temp2=split(temp1.second,r-l+1);
		temp2.first->f+=d;
		temp2.first=Merge(temp2.first,temp2.second);
		update(temp2.first);
		root=Merge(temp1.first,temp2.first);
		update(root);
	}

	int	Query(const int l,const int r)
	{
		pair<node*,node*>temp1=split(root,l);
		pair<node*,node*>temp2=split(temp1.second,r-l+1);
		int	vv=temp2.first->Sum;
		Merge(temp2.first,temp2.second);
		Merge(temp1.first,temp2.first);
		root=temp1.first;
		return vv;
	}
	void	print(node *t)
	{
		if(!t)return ;
		if(t->l)printf("%3ld(%3d:%11d)\t-L>%3ld(%3d:%11d)\n",t-U,t->val,t->key,t->l-U,t->l->val,t->l->key),print(t->l);
		if(t->r)printf("%3ld(%3d:%11d)\t-R>%3ld(%3d:%11d)\n",t-U,t->val,t->key,t->r-U,t->r->val,t->r->key),print(t->r);
	}
	void	print()
	{
		printf("*********************************\n");
		print(root);
	}

}S;

int main()
{
	int	n,m,i,a,x,y,z;
	char	op[10];

	scanf("%d%d",&n,&m);
	for(i=1;i<=n;++i) { scanf("%d",&a); S.insert(i,a); S.print(); }

	while(m--)
	{
		scanf("%s",op);
		if(op[0]=='C')
		{
			scanf("%d%d%d",&x,&y,&z);
			S.add(x,y,z);
		}
		else
		{
			scanf("%d%d",&x,&y);
			printf("%d\n",S.Query(x,y));
		}
	}
	return 0;
}
