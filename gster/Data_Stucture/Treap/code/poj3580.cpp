#pragma GCC optimize(3)
#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <cmath>

using namespace std;

struct Treap
{
	struct node
	{
		int key,val,size,addfl,Sum,Min; bool revfl; node *l,*r;
		node(){key=rand();revfl=false;val=addfl=0;size=1;r=l=0;Min=0x7fffffff;}
	}*root,U[110000];

#define	null	(node*)0
	int	ALL;
	node*	ALLOC(){return U+(++ALL);}
	Treap() { clear(); }
	void	clear() { ALL=0; root=ALLOC(); }

	void	update(node * t)
	{
		push_down(t->l);
		push_down(t->r);
		t->Sum=(t->l?t->l->Sum:0)+(t->r?t->r->Sum:0)+t->val;
		t->size=(t->l?t->l->size:0)+(t->r?t->r->size:0)+1;
		t->Min=min(min((t->l?t->l->Min:0x7fffffff),
					(t->r?t->r->Min:0x7fffffff)),t->val);
		return ;
	}

	void	push_down(node * t)
	{
		if(!t)return ;
		if(t->addfl)
		{
			t->Sum+=t->addfl*t->size;
			t->val+=t->addfl;
			t->Min+=t->addfl;
			if(t->l)t->l->addfl+=t->addfl;
			if(t->r)t->r->addfl+=t->addfl;
			t->addfl=0;
		}
		if(t->revfl)
		{
			swap(t->l,t->r);
			t->revfl^=1;
			if(t->l)t->l->revfl^=1;
			if(t->r)t->r->revfl^=1;
		}
		return ;
	}

	pair<node*,node*>	split(node * t,const int pos)
	{
		if(!t)return make_pair(null,null);
		if(!pos)return make_pair(null,t);
		push_down(t);
		push_down(t->l);
		push_down(t->r);
		if(t->l)
		{
			if(pos==t->l->size)
			{
				node *temp=t->l; t->l=0; update(t);
				return make_pair(temp,t);
			}
			if(pos==t->l->size+1)
			{
				node *temp=t->r; t->r=0; update(t);
				return make_pair(t,temp);
			}
			if(pos<t->l->size)
			{
				pair<node*,node*> temp=split(t->l,pos);
				t->l=temp.second,update(t);
				return make_pair(temp.first,t);
			}
		}
		pair<node*,node*>temp=split(t->r,pos-(t->l?t->l->size:0)-1);
		t->r=temp.first; update(t);
		return make_pair(t,temp.second);
	}

	node*	Merge(node *t1,node* t2)
	{
		if(!t1)return t2; if(!t2)return t1;
		push_down(t1);push_down(t2);
		if(t1->key<t2->key) { t1->r=Merge(t1->r,t2); update(t1); return t1; }
		return  t2->l=Merge(t1,t2->l),update(t2),t2;
	}

	void	Insert(const int pos,const int x)
	{
		pair<node*,node*>temp=split(root,pos);
		node	*nd=ALLOC();
		nd->Sum=nd->val=nd->Min=x;
		root=Merge(Merge(temp.first,nd),temp.second);
		return ;
	}

	void	Erase(const int pos)
	{
		pair<node*,node*>temp1=split(root,pos-1);
		pair<node*,node*>temp2=split(temp1.second,1);
		root=Merge(temp1.first,temp2.second);
		return ;
	}

	void	Reverse(const int l,const int r)
	{
		pair<node*,node*>temp1=split(root,l-1);
		pair<node*,node*>temp2=split(temp1.second,r-l+1);
		temp2.first->revfl^=1;
		root=Merge(temp1.first,Merge(temp2.first,temp2.second));
		return ;
	}

	void	Rotate(const int l,const int r,const int x)
	{
		pair<node*,node*>temp1=split(root,l-1);
		pair<node*,node*>temp2=split(temp1.second,r-l+1);
		pair<node*,node*>temp3=split(temp2.first,r-l+1-x%(r-l+1));
		root=Merge(temp1.first,Merge(Merge(temp3.second,temp3.first),temp2.second));
		return ;
	}

	int	Update_All(node * t)
	{
		if(!t)return 0;
		t->Sum=Update_All(t->l)+Update_All(t->r)+t->val;
		t->size=(t->l?t->l->size:0)+(t->r?t->r->size:0)+1;
		t->Min=min(min((t->l?t->l->Min:0x7fffffff),
					(t->r?t->r->Min:0x7fffffff)),t->val);
		return t->Sum;
	}

	void	Build(const int * A,const int _n)
	{
		node	*tr[_n+2],*stk[_n+2];
		int	top=0;
		for(int i=0;i<_n;++i) tr[i]=ALLOC(),tr[i]->val=A[i];
		for(int i=0;i<_n;++i)
		{
			bool	poped=false;
			while(top && tr[i]->key>stk[top]->key)
			{poped=true;if(top!=1)stk[top-1]->r=stk[top];top--;}
			if(poped)tr[i]->l=stk[top+1];
			stk[++top]=tr[i];
		}
		while(top){if(top!=1)stk[top-1]->r=stk[top];top--;}
		root=stk[1];
		Update_All(root);
		return ;
	}

	void	Add(const int l,const int r,const int x)
	{
		pair<node*,node*>temp1=split(root,l-1);
		pair<node*,node*>temp2=split(temp1.second,r-l+1);
		temp2.first->addfl+=x;
		root=Merge(temp1.first,Merge(temp2.first,temp2.second));
		return ;
	}

	int	Query(const int l,const int r)
	{
		pair<node*,node*>temp1=split(root,l-1);
		pair<node*,node*>temp2=split(temp1.second,r-l+1);
		int	vv=temp2.first->Sum;
		root=Merge(temp1.first,Merge(temp2.first,temp2.second));
		return vv;
	}

	int	Get_Min(const int l,const int r)
	{
		pair<node*,node*>temp1=split(root,l-1);
		pair<node*,node*>temp2=split(temp1.second,r-l+1);
		int	vv=temp2.first->Min;
		root=Merge(temp1.first,Merge(temp2.first,temp2.second));
		return vv;
	}

	void	Dfs_print(node* t)
	{
		if(!t)return ;
		if(t->l) printf("%3d\t%d\t-L>\t%3d\t%d\n",t->val,t->key,t->l->val,t->l->key),Dfs_print(t->l);
		if(t->r) printf("%3d\t%d\t-R>\t%3d\t%d\n",t->val,t->key,t->r->val,t->r->key),Dfs_print(t->r);
	}

	void	print(node* t) { printf("***********************************\n"); Dfs_print(t); }

	void	print() { print(root); }
}S;

int	n,m,a[110000];

int main()
{
	int	i,x,y,z;
	char	op[10];

	scanf("%d",&n);
	for(i=1;i<=n;++i)
		scanf("%d",&a[i]);
	S.Build(a+1,n);
	scanf("%d",&m);
	for(i=1;i<=m;++i)
	{
		scanf("%s",op);
		if(op[0]=='I')
		{
			scanf("%d%d",&x,&y);
			S.Insert(x,y);
		}
		else if(op[0]=='R')
		{
			if(op[3]=='E')
			{
				scanf("%d%d",&x,&y);
				S.Reverse(x,y);
			}
			else
			{
				scanf("%d%d%d",&x,&y,&z);
				S.Rotate(x,y,z);
			}
		}
		else if(op[0]=='D')
		{
			scanf("%d",&x);
			S.Erase(x);
		}
		else if(op[0]=='Q')
		{
			scanf("%d%d",&x,&y);
			printf("%d\n",S.Query(x,y));
		}
		else if(op[0]=='A')
		{
			scanf("%d%d%d",&x,&y,&z);
			S.Add(x,y,z);
		}
		else if(op[0]=='M')
		{
			scanf("%d%d",&x,&y);
			printf("%d\n",S.Get_Min(x,y));
		}
	}
	return 0;
}
