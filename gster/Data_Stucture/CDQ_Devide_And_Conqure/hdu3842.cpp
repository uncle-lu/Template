#include <bits/stdc++.h>
	
using namespace std;

int		n,D;
long long	f[110000];
pair<int,int>	C[110000],A[110000];

struct qr
{
	int	d,p,g,r;
	qr(){}
	qr(const int ar1,const int ar2,const int ar3,const int ar4):
		d(ar1),p(ar2),g(ar3),r(ar4){};
	bool	operator<(const qr& temp)const
	{ return d<temp.d; }
}Q[110000];

typedef	pair<int,int>	PII;
PII	operator-(const PII temp1,const PII temp2)
{ return make_pair(temp1.first-temp2.first,temp1.second-temp2.second); }
int	operator*(const PII temp1,const PII temp2)
{ return 1.0*temp1.first*1.0*temp2.second-1.0*temp1.second*1.0*temp2.first; }

long long H(const int x)
{ return f[x]+1LL*Q[x].r-1LL*Q[x].p-1LL*Q[x].g*(Q[x].d+1LL); }

void	CDQ(const int l,const int r)
{
	if(l>=r)return ;

	int	mid=l+((r-l)>>1);

	CDQ(l,mid);

	int	cnt=0,top=0;
	for(int i=l;i<=mid;++i)
		if(f[i]>=Q[i].p) A[++cnt]=make_pair(Q[i].g,H(i));
	sort(A+1,A+cnt+1);

	for(int i=1;i<=cnt;++i)
	{
		while(cnt>=2 && (C[top]-C[top-1])*(A[i]-C[top])<0)top--;
		C[++top]=A[i];
	}
	
	int	j=0;
	for(int i=mid+1;i<=r;++i)
	{
		while(j<top)
		{
			if(C[j].first*Q[i].d+C[j].second>=
				C[j+1].first*Q[i].d+C[j+1].second)break;
			j++;
		}
		f[i]=max(f[i],1LL*C[j].first*Q[i].d+C[j].second);
	}

	CDQ(mid+1,r);
}

int main()
{
	int		Kase=0;
	while(~scanf("%d%lld%d",&n,&f[0],&D) && (n || f[0] || D))
	{
		for(int i=1;i<=n;++i)
			scanf("%d%d",&Q[i].d,&Q[i].p),
				scanf("%d%d",&Q[i].r,&Q[i].g);

		sort(Q+1,Q+n+1); Q[++n]=qr(D+1,0,0,0);

		for(int i=1;i<=n;++i)f[i]=f[0];

		CDQ(0,n);

		printf("Case %d: %lld\n",++Kase,f[n]);
	}
	return 0;
}
