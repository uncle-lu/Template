#include <bits/stdc++.h>

using namespace std;

int	n,m,Ans[51000],C;
int	Sum[110000];
vector<int>	X,Y,Z;

void	INIT()
{
	memset(Ans,0,sizeof(Ans));
	memset(Sum,0,sizeof(Sum));
	X.clear(),Y.clear(),Z.clear();
}

struct qr
{
	int	tp,x,y,z,cnt,pos,d;
	qr() {}
	qr(const int ar1,const int ar2,const int ar3,const int ar4,const int ar5):
		tp(ar1),x(ar2),y(ar3),z(ar4),cnt(ar5),pos(0),d(0){}
	qr(const int ar1,const int ar2,const int ar3,const int ar4,const int ar5,const int ar6,const int ar7):
		tp(ar1),x(ar2),y(ar3),z(ar4),cnt(ar5),pos(ar6),d(ar7){}

	bool	operator<(const qr& temp)const
	{
		if(x!=temp.x)return x<temp.x;
		if(y!=temp.y)return y<temp.y;
		if(z!=temp.z)return z<temp.z;
		return tp<temp.tp;
	}
}Q[410000],P[410000];

struct qr_
{
	int	tp,x,y,cnt,pos,d;
	qr_&	operator=(const qr& temp)
	{ tp=temp.tp,x=temp.y,y=temp.z,cnt=temp.cnt,pos=temp.pos,d=temp.d;
		return *this; }
	bool	operator<(const qr_& temp)const
	{
		if(x!=temp.x)return x<temp.x;
		if(y!=temp.y)return y<temp.y;
		return tp<temp.tp;
	}
}R[410000],S[410000];

void	Add(int i,const int d) { for(;i<=(int)Z.size();i+=i&-i)Sum[i]+=d; return ; }
int	Query(int i) { int temp=0; for(;i;i-=i&-i)temp+=Sum[i]; return temp; }

void	CDQ_(const int l,const int r)
{
	if(l>=r)return ;
	int	mid=l+((r-l)>>1),ll=l-1,rr=r+1;
	for(int i=l;i<=r;++i)
	{
		if(R[i].tp==1 && R[i].cnt<=mid)Add(R[i].y,1);
		if(R[i].tp==2 && R[i].cnt>mid)Ans[R[i].pos]+=Query(R[i].y)*R[i].d;
		if(R[i].cnt<=mid)S[++ll]=R[i]; else S[--rr]=R[i];
	}

	for(int i=l;i<=r;++i)
	{
		if(R[i].tp==1 && R[i].cnt<=mid)Add(R[i].y,-1);
		if(i<=ll)R[i]=S[i]; else R[i]=S[r-i+rr];
	}
	CDQ_(l,ll);CDQ_(rr,r); return ;
}

void	CDQ(const int l,const int r)
{
	if(l>=r)return ;

	int	mid=l+((r-l)>>1),ll=l-1,rr=r+1,tmp=0;

	for(int i=l;i<=r;++i)
	{
		if(Q[i].tp==1 && Q[i].cnt<=mid)++tmp,R[tmp]=Q[i],R[tmp].cnt=tmp;
		if(Q[i].tp==2 && Q[i].cnt>mid)++tmp,R[tmp]=Q[i],R[tmp].cnt=tmp;
		if(Q[i].cnt<=mid)P[++ll]=Q[i]; else P[--rr]=Q[i];
	}

	sort(R+1,R+tmp+1);
	CDQ_(1,tmp);

	for(int i=l;i<=r;++i)
	{
		if(i<=ll)Q[i]=P[i];
		else Q[i]=P[r-(i-rr)];
	}
	CDQ(l,ll);CDQ(rr,r); return ;
}

int main()
{
	int	T,i,op,x,y,z,s,t,u,cnt;

	scanf("%d",&T);
	while(T--)
	{
		cnt=0; scanf("%d",&n);
		INIT();
		for(i=1;i<=n;++i)
		{
			scanf("%d",&op);
			if(op==1)
			{
				scanf("%d%d%d",&x,&y,&z);
				++cnt,Q[cnt]=qr(1,x,y,z,cnt);
				X.push_back(x),Y.push_back(y),Z.push_back(z);
			}
			else
			{
				scanf("%d%d%d%d%d%d",&x,&y,&z,&s,&t,&u);
				//if(x>s)swap(x,s);if(y>t)swap(y,t);if(z>u)swap(z,u);
				++cnt,Q[cnt]=qr(2,s  ,t  ,u  ,cnt,++Ans[0],+1);
				++cnt,Q[cnt]=qr(2,s  ,t  ,z-1,cnt,  Ans[0],-1);
				++cnt,Q[cnt]=qr(2,s  ,y-1,u  ,cnt,  Ans[0],-1);
				++cnt,Q[cnt]=qr(2,s  ,y-1,z-1,cnt,  Ans[0],+1);
				++cnt,Q[cnt]=qr(2,x-1,t  ,u  ,cnt,  Ans[0],-1);
				++cnt,Q[cnt]=qr(2,x-1,t  ,z-1,cnt,  Ans[0],+1);
				++cnt,Q[cnt]=qr(2,x-1,y-1,u  ,cnt,  Ans[0],+1);
				++cnt,Q[cnt]=qr(2,x-1,y-1,z-1,cnt,  Ans[0],-1);
				X.push_back(x-1),Y.push_back(y-1),Z.push_back(z-1);
				X.push_back(s),Y.push_back(t),Z.push_back(u);
			}
		}

		sort(X.begin(),X.end());X.erase(unique(X.begin(),X.end()),X.end());
		sort(Y.begin(),Y.end());Y.erase(unique(Y.begin(),Y.end()),Y.end());
		sort(Z.begin(),Z.end());Z.erase(unique(Z.begin(),Z.end()),Z.end());

		for(i=1;i<=cnt;++i)
		{
			Q[i].x=lower_bound(X.begin(),X.end(),Q[i].x)-X.begin()+1;
			Q[i].y=lower_bound(Y.begin(),Y.end(),Q[i].y)-Y.begin()+1;
			Q[i].z=lower_bound(Z.begin(),Z.end(),Q[i].z)-Z.begin()+1;
		}

		sort(Q+1,Q+cnt+1);
		CDQ(1,cnt);

		for(i=1;i<=Ans[0];++i)printf("%d\n",Ans[i]);
	}

	return 0;
}
