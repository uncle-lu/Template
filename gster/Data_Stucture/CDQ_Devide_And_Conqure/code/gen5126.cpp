#include <bits/stdc++.h>

using namespace std;

const int	LIM=1e9;

int main(int argc,char ** argv)
{
	assert(argc>=3);
	int	T=atoi(argv[1]),n=atoi(argv[2]);
	random_device	seed;
	mt19937		RND(seed());
	cout << T << endl;
	while(T--)
	{
		cout << n << endl;
		for(int i=1;i<=n;++i)
		{
			int	op=RND()%2;
			if(i==n)op=0;
			if(op)
			{
				cout << 1 << ' ' << RND()%LIM+1 << ' ' << RND()%LIM+1 << ' ' << RND()%LIM+1 << endl;
			}
			else
			{
				int	x,y,z,s,t,u;
				x=RND()%LIM+1; y=RND()%LIM+1; z=RND()%LIM+1; s=RND()%LIM+1; t=RND()%LIM+1; u=RND()%LIM+1;
				if(x>s)swap(x,s);if(y>t)swap(y,t);if(z>u)swap(z,u);
				printf("2 %d %d %d %d %d %d\n",x,y,z,s,t,u);
			}
		}
	}
	return 0;
}
