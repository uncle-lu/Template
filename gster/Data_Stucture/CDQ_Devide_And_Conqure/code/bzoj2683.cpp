#include <bits/stdc++.h>

using namespace std;

struct qr
{
	int	tp,x,y,d,cnt,pos; qr() {}
	qr(int ar1,int ar2,int ar3,int ar4,int ar5,int ar6):
		tp(ar1),x(ar2),y(ar3),d(ar4),cnt(ar5),pos(ar6){}
	bool	operator<(const qr& temp)const
	{
		if(x!=temp.x)return x<temp.x;
		if(y!=temp.y)return y<temp.y;
		return tp<temp.tp;
	}
}Q[810000],P[810000];

int	n,cnt;
int	S[510000],Ans[210000];

void	Add(int i,const int d) { if(!i)return ; for(;i<=n;i+=i&-i)S[i]+=d; }
int	Query(int i) { int temp=0; for(;i;i-=i&-i)temp+=S[i]; return temp; }

void	CDQ(const int l,const int r)
{
	if(l==r) return ;
	int	i,mid=l+((r-l)>>1),ll=l-1,rr=r+1;
	for(i=l;i<=r;++i)
	{
		if(Q[i].cnt<=mid && Q[i].tp==1) Add(Q[i].y,Q[i].d);
		if(Q[i].cnt>mid  && Q[i].tp==2) Ans[Q[i].pos]+=Query(Q[i].y)*Q[i].d;
		if(Q[i].cnt<=mid) P[++ll]=Q[i];
		if(Q[i].cnt>mid)  P[--rr]=Q[i];
	}
	for(i=l;i<=r;++i)
	{
		if(Q[i].cnt<=mid && Q[i].tp==1) Add(Q[i].y,-Q[i].d);
		if(i<=ll)Q[i]=P[i];
		else	Q[i]=P[r-(i-rr)];
	}
	CDQ(l,mid);CDQ(mid+1,r);
	return ;
}

int main()
{
	int	x,y,z,zz,i,op;
	scanf("%d",&n);
	while(true)
	{
		scanf("%d",&op);
		if(op==3)break;
		if(op==1)
		{
			scanf("%d%d%d",&x,&y,&z);
			cnt++;Q[cnt]=qr(1,x,y,z,cnt,0);
		}
		else
		{
			scanf("%d%d%d%d",&x,&y,&z,&zz);
			cnt++;Q[cnt]=qr(2,x-1,y-1,1,cnt,++Ans[0]);
			cnt++;Q[cnt]=qr(2,x-1,zz,-1,cnt,Ans[0]);
			cnt++;Q[cnt]=qr(2,z,y-1,-1,cnt,Ans[0]);
			cnt++;Q[cnt]=qr(2,z,zz,1,cnt,Ans[0]);
		}
	}

	sort(Q+1,Q+cnt+1);

	CDQ(1,cnt);

	for(i=1;i<=Ans[0];++i)
		printf("%d\n",Ans[i]);

	return 0;
}

