#include <bits/stdc++.h>

using namespace std;

struct Edge_base { int	to,next; }e[140000];
int	cnt,p[51000];
int	n,m,q,ind,a[51000];
int	Head[51000],Pre[51000],Dfn[51000],hv[51000];
int	Sum[140000],f[140000];

void	INIT()
{
	cnt=ind=0;
	memset(p,0,sizeof(p));
	memset(Head,0,sizeof(Head));
	memset(hv,0,sizeof(hv));
	memset(Sum,0,sizeof(Sum));
	memset(f,0,sizeof(f));
}

void	Add_edge(const int x,const int y)
{
	e[++cnt].to=y;
	e[cnt].next=p[x];
	p[x]=cnt;
	return ;
}

int	Init_Dfs(const int S,const int fa)
{
	int	s=1,Max=0;
	for(int i=p[S];i;i=e[i].next)
	{
		if(e[i].to==fa)continue;
		int temp=Init_Dfs(e[i].to,S);
		if(temp>Max)Max=temp,hv[S]=e[i].to;
		s+=temp;
	}
	return s;
}

void	Dfs(const int S,const int fa)
{
	if(!Head[S])Head[S]=Head[fa];Dfn[S]=++ind;
	if(hv[S])Dfs(hv[S],S);
	for(int i=p[S];i;i=e[i].next)
	{
		if(e[i].to==fa)continue;
		if(e[i].to==hv[S])continue;
		Head[e[i].to]=e[i].to;
		Pre[e[i].to]=S;
		Dfs(e[i].to,S);
	}
	return ;
}

void	push_down(const int num,const int l,const int r)
{ if(f[num]) { Sum[num]+=f[num]*(r-l+1); if(l!=r) { f[num<<1]+=f[num]; f[num<<1|1]+=f[num]; } f[num]=0; } return ; }
void	update(const int num) { Sum[num]=Sum[num<<1]+Sum[num<<1|1]; return ; }

void	Add(const int l,const int r,const int num,const int s,const int t,const int d)
{
	if(s<=l && r<=t) { f[num]+=d; push_down(num,l,r); return ; }
	int	mid=l+((r-l)>>1);
	push_down(num,l,r);
	if(s<=mid)Add(l,mid,num<<1,s,t,d);
	if(t>mid)Add(mid+1,r,num<<1|1,s,t,d);
	update(num);
	return ;
}

int	Query(const int l,const int r,const int num,const int s)
{
	if(l==r) { push_down(num,l,r); return Sum[num]; }
	int	mid=l+((r-l)>>1); push_down(num,l,r);
	if(s<=mid)return Query(l,mid,num<<1,s);
	return Query(mid+1,r,num<<1|1,s);
}

void	Increase(int x,int y,const int z)
{
	while(Head[x]!=Head[y])
	{
		if(Dfn[Head[x]]>Dfn[Head[y]])
		{
			Add(1,n,1,Dfn[Head[x]],Dfn[x],z);
			x=Pre[Head[x]];
		}
		else
		{
			Add(1,n,1,Dfn[Head[y]],Dfn[y],z);
			y=Pre[Head[y]];
		}
	}
	Dfn[x]<Dfn[y]?Add(1,n,1,Dfn[x],Dfn[y],z):Add(1,n,1,Dfn[y],Dfn[x],z);
	return ;
}

int main()
{
	int	i,x,y,z;
	char	op[10];
	while(~scanf("%d%d%d",&n,&m,&q))
	{
		INIT();
		for(i=1;i<=n;++i) scanf("%d",&a[i]);
		for(i=1;i<n;++i)
		{
			scanf("%d%d",&x,&y);
			Add_edge(x,y);Add_edge(y,x);
		}

		Init_Dfs(1,0);
		Head[1]=1;
		Dfs(1,1);

		for(i=1;i<=n;++i) Add(1,n,1,Dfn[i],Dfn[i],a[i]);

		while(q--)
		{
			scanf("%s",op);
			if(op[0]=='I')
			{
				scanf("%d%d%d",&x,&y,&z);
				Increase(x,y,z);
			}
			if(op[0]=='D')
			{
				scanf("%d%d%d",&x,&y,&z);
				Increase(x,y,-z);
			}
			if(op[0]=='Q')
			{
				scanf("%d",&x);
				printf("%d\n",Query(1,n,1,Dfn[x]));
			}
		}
	}

	return 0;
}
