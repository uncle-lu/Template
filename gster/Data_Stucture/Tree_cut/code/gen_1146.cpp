#include <bits/stdc++.h>

using namespace std;

int main(int	argc,char** argv)
{
	int	i,n=atoi(argv[1]),m=atoi(argv[2]),t=ceil(log(n)/log(2.0)*2.0);
	vector<int>	vec;
	random_device	seed;
	mt19937		RND(seed());

	cout << n << ' ' << m << endl;
	for(i=1;i<=n;++i) { cout << RND()%(int)10 << ' '; }
	cout << endl;
	for(i=1;i<=n;++i) vec.push_back(i);
	shuffle(vec.begin(),vec.end(),mt19937(seed()));
	for(i=1;i<(int)vec.size();++i)
		cout << vec[RND()%i] << ' ' << vec[i] << endl;
	while(m--)
	{
		int	op=RND()%10;
		if(m==0) op=9;
		if(op<=1)
		{
			cout << 0 << ' ' << RND()%n+1 << ' ' << RND()%(int)10 << endl;
		}
		else
		{
			cout << RND()%t+1 << ' ' << RND()%n+1 << ' ' << RND()%n+1 << endl;
		}
	}
	return 0;
}
