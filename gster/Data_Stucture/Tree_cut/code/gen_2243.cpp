#include <bits/stdc++.h>

using namespace std;

int main(int argc,char **argv)
{
	random_device	seed;
	mt19937		RND(seed());
	int	n=atoi(argv[1]),m=atoi(argv[2]);
	int	col_knd=ceil(log(log(n)))+1;
	vector<int>	pnt;
	cout << n << ' ' << m << endl;
	for(int i=1;i<=n;++i)
	{
		cout << RND()%col_knd+1 << ' ';
		pnt.push_back(i);
	}
	cout << endl;
	shuffle(pnt.begin(),pnt.end(),mt19937(seed()));
	for(int i=1;i<(int)pnt.size();++i)
	{
		cout <<  pnt[RND()%i] << ' ' << pnt[i] << endl;
	}

	while((m--)!=1)
	{
		int	op=RND()%100;
		if(op<40)
		{
			cout << "Q ";
			int	y=RND()%n+1,x=RND()%y+1;
			cout << x << ' ' << y << endl;
		}
		else
		{
			cout << "C ";
			int	y=RND()%n+1,x=RND()%y+1;
			cout << x << ' ' << y << ' ' << RND()%col_knd+1 << endl;
		}
	}

	cout << "Q ";
	int	y=RND()%n+1,x=RND()%y+1;
	cout << x << ' ' << y << endl;

	return 0;
}
