#include <bits/stdc++.h>

using namespace std;

int main()
{
	random_device	seed;
	mt19937		RND(seed());
	int	n=20,m=20,lim=10,i;
	cout << n << ' ' << m << endl;
	for(i=1;i<=n;++i)
		cout << (int)RND()%1001 << ' ';
	cout << endl;

	while(m--)
	{
		int op=RND()%8;
		if(n==0)
		{
			cout << "INSERT ";
			cout << 0 << ' ';
			int t=RND()%lim+1;
			cout << t << ' ';
			for(i=1;i<=t;++i)
				cout << (int)RND()%1001 << ' ';
			cout << endl;n+=t;
		}
		if(op==0)
		{
			cout << "INSERT ";
			cout << RND()%n+1 << ' ';
			int t=RND()%lim+1;
			cout << t << ' ';
			for(i=1;i<=t;++i)
				cout << (int)RND()%1001 << ' ';
			cout << endl;n+=t;
		}
		if(op==1)
		{
			cout << "DELETE ";
			int y=RND()%n+1,x=RND()%y+1;
			cout << x << ' ' << y-x+1 << endl;
			n-=y-x+1;
		}
		if(op==2)
		{
			cout << "MAKE_SAME ";
			int y=RND()%n+1,x=RND()%y+1;
			cout << x << ' ' << y-x+1 << ' ' << (int)RND()%1001 << endl;
		}
		if(op==3)
		{
			cout << "REVERSE ";
			int y=RND()%n+1,x=RND()%y+1;
			cout << x << ' ' << y-x+1 << endl;
		}
		if(op>=4 && op<=6)
		{
			cout << "GET_SUM ";
			int y=RND()%n+1,x=RND()%y+2;
			cout << x << ' ' << y-x+1 << endl;
		}
		if(op>6)
		{
			cout << "MAX_SUM" << endl;
		}
	}

	return 0;
}
