#pragma GCC optimize(3)
#include <bits/stdc++.h>

using namespace std;

//#define   Debug

struct Splay
{
#define inf 0x3f3f3f3f
	struct node
	{
		int val,size,chgf,revf,ls,rs,ms,Sum;
		node    *s[2],*fa;
		node() {}
		node(const int x) { Sum=ms=val=x;size=1;chgf=inf; ls=rs=(x>=0?x:0); revf=0;s[0]=s[1]=fa=0; }
		bool    getlr() { return fa->s[1]==this; }
		node*   link(const int w,node * t) {  if((s[w]=t))t->fa=this; return this; }
		void    update()
		{
			Sum=(s[0]?s[0]->Sum:0)+(s[1]?s[1]->Sum:0)+val;
			size=(s[0]?s[0]->size:0)+(s[1]?s[1]->size:0)+1;
			ms=max(max((s[0]?s[0]->ms:-inf),(s[1]?s[1]->ms:-inf)),(s[0]?s[0]->rs:0)+(s[1]?s[1]->ls:0)+val);
			ls=max((s[0]?s[0]->ls:0),(s[0]?s[0]->Sum:0)+val+(s[1]?s[1]->ls:0));
			rs=max((s[1]?s[1]->rs:0),(s[1]?s[1]->Sum:0)+val+(s[0]?s[0]->rs:0));
			return ;
		}
		void    pd()
		{
			if(s[0])s[0]->push_down();
			if(s[1])s[1]->push_down();
		}
		void    push_down()
		{
			if(chgf!=inf)
			{
				Sum=chgf*size;val=chgf;ls=rs=(chgf>=0?Sum:0);ms=(chgf>=0?Sum:chgf);
				if(s[0]) { s[0]->chgf=chgf; }
				if(s[1]) { s[1]->chgf=chgf; }
				chgf=inf; revf=0;
			}
			if(revf)
			{
				if(s[0])s[0]->revf^=1; if(s[1])s[1]->revf^=1;
				swap(s[0],s[1]);swap(ls,rs);revf=0;
			}
		}
	}*root,U[510000],*Trash[510000];
	int ALL,top;
	void    unlink(node * t,const int w)
	{
		RECYCLE(t->s[w]);
		t->s[w]=0;
	}

	node*   ALLOC(const int x)
	{
		node* nd=top?Trash[top--]:U+(++ALL);
		*nd=node(x);
		return nd;
	}
	void    RECYCLE(node * t)
	{
		if(t->s[0])RECYCLE(t->s[0]);
		if(t->s[1])RECYCLE(t->s[1]);
		Trash[++top]=t;
	}

	Splay()
	{
		clear();
	}
	void    clear()
	{
		ALL=top=0;
		root=ALLOC(-inf);
		root->link(1,ALLOC(-inf));
		root->update();
	}

	void    rot(node * t)
	{
		node *gfa=t->fa->fa;
		t->getlr()?t->link(0,t->fa->link(1,t->s[0])): t->link(1,t->fa->link(0,t->s[1]));
		t->fa->update();
		if(gfa)gfa->link(gfa->s[1]==t->fa,t);
		else    t->fa=0,root=t;
	}

	void    splay(node * t,node * tar)
	{
		while(t->fa!=tar && t->fa->fa!=tar)
			t->getlr()==t->fa->getlr()?(rot(t->fa),rot(t)):(rot(t),rot(t));
		if(t->fa!=tar)rot(t);
		t->update();
	}

	node*   find(int k)
	{
		node *t=root;
		int w=t->s[0]?t->s[0]->size:0;
		while(t->pd(),w+1!=k)
		{
			if(w>=k)t=t->s[0];
			else k-=w+1,t=t->s[1];   
			w=t->s[0]?t->s[0]->size:0;
		}
		return t;
	}

	node*   build(const int * a,const int _n)
	{
		node *nd=ALLOC(a[0]),*cur=nd;
		for(int i=1; i<_n; ++i)
		{
			node *ndd=ALLOC(a[i]);
			cur->link(1,ndd);
			cur=ndd;
		}
		while(cur)
			cur->update(),cur=cur->fa;
		return nd;
	}
	void    split(const int l,const int r)
	{
		splay(find(l),0);
		splay(find(r+2),root);
	}
	void    insert(const int pos,const int * a,const int _n)
	{
		split(pos,pos-1);
		root->s[1]->link(0,build(a,_n));
		root->s[1]->update();
		root->update();
	}
	void    erase(const int l,const int r)
	{
		split(l,r-1);
		unlink(root->s[1],0);
		root->s[1]->update();
		root->update();
	}
	void    change(const int l,const int r,const int d)
	{
		split(l,r-1);
		root->s[1]->s[0]->chgf=d;
		root->s[1]->s[0]->push_down();
		root->s[1]->update();
		root->update();
	}
	void    reverse(const int l,const int r)
	{
		split(l,r-1);
		root->s[1]->s[0]->revf^=1;
		root->s[1]->s[0]->push_down();
		root->s[1]->update();
		root->update();
	}
	int get_sum(const int l,const int r)
	{
		split(l,r-1);
		root->s[1]->s[0]->push_down();
		root->s[1]->update();
		root->update();
		return root->s[1]->s[0]->Sum;
	}
	int max_sum()
	{
		split(1,root->size-2);
		return root->s[1]->s[0]->ms;
	}

#ifdef  Debug
	void    print(node * t)
	{
		if(t->s[0])printf("%d\t-L>\t%d\n",t->val,t->s[0]->val),print(t->s[0]);
		if(t->s[1])printf("%d\t-R>\t%d\n",t->val,t->s[1]->val),print(t->s[1]);
	}
	void    print()
	{
		printf("**************************************\n");
		print(root);
	}
#endif
} S;

int a[510000];

int getint()
{
	int data=0,f=1;char ch=getchar();
	while(ch>'9' || ch<'0'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0' && ch<='9')data=data*10+ch-48,ch=getchar();
	return data*f;
}

int main()
{
	int n,m,i,x,y,z;
	char    op[15];

	n=getint();
	m=getint();
	for(i=1; i<=n; ++i) a[i]=getint();
	S.insert(1,a+1,n);
#ifdef  Debug
	S.print();
#endif
	while(m--)
	{
		scanf("%s",op);
		if(op[2]=='S')//INSERT
		{
			x=getint();y=getint();
			for(i=1; i<=y; ++i)a[i]=getint();
			S.insert(x+1,a+1,y);
		}
		if(op[2]=='L')//DELETE
		{
			x=getint();y=getint();
			S.erase(x,x+y);
		}
		if(op[2]=='K')//MAKE_SAME
		{
			x=getint();y=getint();z=getint();
			S.change(x,x+y,z);
		}
		if(op[2]=='V')//REVERSE
		{
			x=getint();y=getint();
			S.reverse(x,x+y);
		}
		if(op[2]=='T')//GET_SUM
		{
			x=getint();
			y=getint();
			printf("%d\n",y?S.get_sum(x,x+y):0);
		}
		if(op[2]=='X')//MAX_SUM
		{
			printf("%d\n",S.max_sum());
		}
		//for(i=1;i<=S.root->size-2;++i)
		//S.find(i+1);
#ifdef  Debug
		//printf("$$$$$$$$$$$$$$$$$$$$$\n");
		//for(i=1; i<=S.root->size-2; ++i)
		//printf("%d ",S.find(i+1)->val);
		//printf("\n$$$$$$$$$$$$$$$$$$$$$\n");
#endif
	}
	return 0;
}
