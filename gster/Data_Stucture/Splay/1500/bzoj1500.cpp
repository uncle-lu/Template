#pragma GCC optimize(3)
#include <bits/stdc++.h>
#define inf 0x3f3f3f3f
#define ll s[0]
#define rr s[1]
using namespace std;
struct SS
{
	struct lf
	{
		int v,sz,cf,rf,ls,rs,ms,sm;
		lf*s[2],*fa;
		lf() {}
		lf(int x)
		{
			sm=ms=v=x;
			sz=1;
			cf=inf;
			ls=rs=(x>0?x:0);
			rf=0;
			ll=rr=fa=0;
		}
		bool g()
		{
			return fa->rr==this;
		}
		lf*lk(int w,lf*t)
		{
			if((s[w]=t))t->fa=this;
			return this;
		}
		void up()
		{
			sm=(ll?ll->sm:0)+(rr?rr->sm:0)+v;
			sz=(ll?ll->sz:0)+(rr?rr->sz:0)+1;
			ms=max(max((ll?ll->ms:-inf),(rr?rr->ms:-inf)),(ll?ll->rs:0)+(rr?rr->ls:0)+v);
			ls=max((ll?ll->ls:0),(ll?ll->sm:0)+v+(rr?rr->ls:0));
			rs=max((rr?rr->rs:0),(rr?rr->sm:0)+v+(ll?ll->rs:0));
			return;
		}
		void pd()
		{
			if(ll)ll->pn();
			if(rr)rr->pn();
		}
		void pn()
		{
			if(cf!=inf)
			{
				sm=cf*sz;
				v=cf;
				ls=rs=(cf>=0?sm:0);
				ms=(cf>=0?sm:cf);
				if(ll)
				{
					ll->cf=cf;
				}
				if(rr)
				{
					rr->cf=cf;
				}
				cf=inf;
				rf=0;
			}
			if(rf)
			{
				if(ll)ll->rf^=1;
				if(rr)rr->rf^=1;
				swap(ll,rr);
				swap(ls,rs);
				rf=0;
			}
		}
	}*rt,U[510000],*Tr[510000],*AL;
	int tp;
	void uk(lf*t,int w)
	{
		re(t->s[w]);
		t->s[w]=0;
	}
	lf*al(int x)
	{
		lf*nd=tp?Tr[tp--]:++AL;
		*nd=lf(x);
		return nd;
	}
	void re(lf*t)
	{
		if(t->ll)re(t->ll);
		if(t->rr)re(t->rr);
		Tr[++tp]=t;
	}
	SS()
	{
		clear();
	}
	void clear()
	{
		AL=U;
		tp=0;
		rt=al(-inf);
		rt->lk(1,al(-inf));
		rt->up();
	}
	void ot(lf*t)
	{
		lf*gfa=t->fa->fa;
		t->g()?t->lk(0,t->fa->lk(1,t->ll)): t->lk(1,t->fa->lk(0,t->rr));
		t->fa->up();
		if(gfa)gfa->lk(gfa->rr==t->fa,t);
		else t->fa=0,rt=t;
	}
	void sy(lf*t,lf*ar)
	{
		while(t->fa!=ar && t->fa->fa!=ar)
			t->g()==t->fa->g()?(ot(t->fa),ot(t)):(ot(t),ot(t));
		if(t->fa!=ar)ot(t);
		t->up();
	}
	lf*fd(int k)
	{
		lf*t=rt;
		int w=t->ll?t->ll->sz:0;
		while(t->pd(),w+1!=k)
		{
			if(w>=k)t=t->ll;
			else k-=w+1,t=t->rr;
			w=t->ll?t->ll->sz:0;
		}
		return t;
	}
	lf*build(int*a,int _n)
	{
		lf*nd=al(a[0]),*cr=nd;
		for(int i=1; i<_n; ++i)
		{
			lf*dd=al(a[i]);
			cr->lk(1,dd);
			cr=dd;
		}
		while(cr) cr->up(),cr=cr->fa;
		return nd;
	}
	void sp(int l,int r)
	{
		sy(fd(l),0);
		sy(fd(r+2),rt);
	}
	void in(int pos,int*a,int _n)
	{
		sp(pos,pos-1);
		rt->rr->lk(0,build(a,_n));
		rt->rr->up();
		rt->up();
	}
	void es(int l,int r)
	{
		sp(l,r-1);
		uk(rt->rr,0);
		rt->rr->up();
		rt->up();
	}
	void cg(int l,int r,int d)
	{
		sp(l,r-1);
		rt->rr->ll->cf=d;
		rt->rr->ll->pn();
		rt->rr->up();
		rt->up();
	}
	void rv(int l,int r)
	{
		sp(l,r-1);
		rt->rr->ll->rf^=1;
		rt->rr->ll->pn();
		rt->rr->up();
		rt->up();
	}
	int gs(int l,int r)
	{
		sp(l,r-1);
		rt->rr->ll->pn();
		rt->rr->up();
		rt->up();
		return rt->rr->ll->sm;
	}
	int mm()
	{
		sp(1,rt->sz-2);
		return rt->rr->ll->ms;
	}
}
S;
int a[510000];
int rd()
{
	int dt=0,f=1;
	char ch=getchar();
	while(ch>57||ch<48)
	{
		if(ch=='-')f=-1;
		ch=getchar();
	}
	while(ch>='0' && ch<='9')dt=dt*10+ch-48,ch=getchar();
	return dt*f;
}
int main()
{
	int n,m,i,x,y,z;
	char op[15];
	n=rd();
	m=rd();
	for(i=1; i<=n; ++i) a[i]=rd();
	S.in(1,a+1,n);
	while(m--)
	{
		scanf("%s",op);
		if(op[2]=='S')
		{
			x=rd();
			y=rd();
			for(i=1; i<=y; ++i)a[i]=rd();
			S.in(x+1,a+1,y);
		}
		if(op[2]=='L')
		{
			x=rd();
			y=rd();
			S.es(x,x+y);
		}
		if(op[2]=='K')
		{
			x=rd();
			y=rd();
			z=rd();
			S.cg(x,x+y,z);
		}
		if(op[2]=='V')
		{
			x=rd();
			y=rd();
			S.rv(x,x+y);
		}
		if(op[2]=='T')
		{
			x=rd();
			y=rd();
			printf("%d\n",y?S.gs(x,x+y):0);
		}
		if(op[2]=='X')
		{
			printf("%d\n",S.mm());
		}
	}
	return 0;
}
