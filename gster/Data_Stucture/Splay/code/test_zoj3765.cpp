#include <bits/stdc++.h>

using namespace std;

int	gcd(int a,int b)
{
	if(a<b)swap(a,b);
	if(b==0)return a;
	for(int c=a%b;c;a=b,b=c,c=a%b);
	return b;
}

int	n,m;
int	a[310000],b[310000];

int main()
{
	int	i,j,x,y,z;
	char	op[10];
	while(scanf("%d%d",&n,&m)==2)
	{
		for(i=1;i<=n;++i)scanf("%d%d",&a[i],&b[i]);
		for(i=1;i<=m;++i)
		{
			scanf("%s",op);
			if(op[0]=='Q')
			{
				scanf("%d%d%d",&x,&y,&z);
				int temp=0;
				for(j=x;j<=y;++j)
					if(b[j]==z)temp=gcd(temp,a[j]);
				printf("%d\n",temp?temp:-1);
			}
			if(op[0]=='I')
			{
				scanf("%d%d%d",&x,&y,&z);
				for(j=n++;j>x;--j)a[j+1]=a[j],b[j+1]=b[j];
				a[x+1]=y,b[x+1]=z;
			}
			if(op[0]=='D')
			{
				scanf("%d",&x);
				for(j=x;j<=n;++j)a[j]=a[j+1],b[j]=b[j+1];
				n--;
			}
			if(op[0]=='M')
			{
				scanf("%d%d",&x,&y);
				a[x]=y;
			}
			if(op[0]=='R')
			{
				scanf("%d",&x);
				b[x]^=1;
			}
		}
	}
	return 0;
}
