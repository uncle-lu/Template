#include <bits/stdc++.h>

using namespace std;

int main()
{
	int	n,m,x,y,T;
	random_device	seed;
	mt19937		RND(seed());
	n=20000,m=10000;T=5;
	while(T--)
	{
		cout << n << ' ' << m << endl;
		for(int i=1;i<=n;++i)
		{
			cout << RND()%100+1 << ' ' << RND()%2 << endl;
		}
		for(int i=1;i<=m;++i)
		{
			int	op=RND()%10;
			switch(op)
			{
				case 0:
					cout << "M " << RND()%n+1 << ' ' << RND()%100+1 << endl;
					break;
				case 1:
					cout << "I " << RND()%n+1 << ' ' << RND()%100+1 << ' ' << RND()%2 << endl;
					n++;
					break;
				case 2:
					cout << "D " << RND()%n+1 << endl;
					n--;
					break;
				case 3:
					cout << "R " << RND()%n+1 << endl;
					break;
				default:
					y=RND()%n+1,x=RND()%y+1;
					cout << "Q " << x << ' ' << y << ' ' << RND()%2 << endl;
			}
		}
	}
	return 0;
}
