#include <bits/stdc++.h>

using namespace std;

struct Splay
{
	struct node
	{
		int	val,size,f;
		node	*s[2],*fa;
		node() { fa=s[0]=s[1]=0;size=1;val=f=0; }
		node(const int _val):val(_val) { fa=s[0]=s[1]=0;size=1;f=0; }
		bool	getlr() { return fa->s[1]==this; }
		node*	link(const int w,node * t) { s[w]=t;if(t)t->fa=this;return this; }
		void	update() { size=1+(s[0]?s[0]->size:0)+(s[1]?s[1]->size:0); return ; }
		void	push_down()
		{
			if(f)
			{
				swap(s[0],s[1]);
				if(s[0])s[0]->f^=1;
				if(s[1])s[1]->f^=1;
				f=0;
			}
		}
	}*root,U[310000],*Trash[310000];int ALL,top;

	node*	ALLOC(const int x) { node *nd=new(top?Trash[top--]:U+(++ALL))node;*nd=node(x);return nd; }
	void	RECYCLE(node * t) { Trash[++top]=t; }
	void	unlink(node * t,const int w) { RECYCLE(t->s[w]);t->s[w]=0;return ; }

	Splay() { clear(); }
	void	clear() { ALL=top=0;root=ALLOC(-1); node *nd=ALLOC(-2); root->link(1,nd); root->update(); }

	void	rot(node * t)
	{
		node *gfa=t->fa->fa;
		t->getlr()?t->link(0,t->fa->link(1,t->s[0])):
			t->link(1,t->fa->link(0,t->s[1]));
		t->fa->update();
		if(gfa)gfa->link(gfa->s[1]==t->fa,t);
		else	t->fa=0,root=t;
	}

	void	splay(node * t,node * tar)
	{
		while(t->fa!=tar && t->fa->fa!=tar) t->getlr()==t->fa->getlr()?  (rot(t->fa),rot(t)):(rot(t),rot(t));
		if(t->fa!=tar)rot(t); t->update();
	}

	node*	find(int k)
	{
		node *t=root; t->push_down(); int w=t->s[0]?t->s[0]->size:0; while(w+1!=k)
		{ if(w>=k)t=t->s[0]; else k-=w+1,t=t->s[1]; t->push_down(); w=t->s[0]?t->s[0]->size:0; } return t;
	}
	
	void	split(const int l,const int r)
	{ splay(find(l),0);splay(find(r+2),root); }
	void	insert(const int pos,const int d)
	{ split(pos,pos-1);root->s[1]->link(0,ALLOC(d)); root->s[1]->update(); root->update(); }
	void	cut(const int l,const int r,const int pos)
	{ split(l,r); node *temp=root->s[1]->s[0]; unlink(root->s[1],0); split(pos,pos-1); root->s[1]->link(0,temp);
		root->s[1]->update(); root->update(); }
	void	reverse(const int l,const int r) { split(l,r); root->s[1]->s[0]->f^=1; }
	int	operator[](const int pos) { return find(pos+1)->val; }

	void	pr(node * t)
	{
		if(t->s[0])printf("%d:%d\t-L>\t%d:%d\n",t->val,t->f,t->s[0]->val,t->s[0]->f),pr(t->s[0]);
		if(t->s[1])printf("%d:%d\t-R>\t%d:%d\n",t->val,t->f,t->s[1]->val,t->s[1]->f),pr(t->s[1]);
	}
	void	print(node * t) { printf("************************************************\n"); pr(t); }
	void	print() { print(root); }
}S;

int	n,m;

int main()
{
	int	i,x,y,z;
	char	op[10];
	while(scanf("%d%d",&n,&m)==2 && n!=-1 && m!=-1)
	{
		S.clear();
		for(i=1;i<=n;++i)S.insert(i,i);
		for(i=1;i<=m;++i)
		{
			scanf("%s",op);
			if(op[0]=='F') { scanf("%d%d",&x,&y); S.reverse(x,y); }
			else { scanf("%d%d%d",&x,&y,&z); S.cut(x,y,z+1); }
		}
		for(i=1;i<=n;++i){printf("%d",S[i]);if(i!=n)printf(" ");}
		printf("\n");
	}
	return 0;
}
