#include <bits/stdc++.h>

using namespace std;

struct Splay
{
	struct node
	{
		node *fa,*s[2];
		int	val,size,gcd[2],st;
		node() { fa=s[0]=s[1]=0; size=1,val=st=0; gcd[0]=gcd[1]=0; }
		node(const int _val,const int _st):val(_val),st(_st)
		{ fa=s[0]=s[1]=0;size=1; gcd[st]=val,gcd[!st]=0; }
		bool	getlr() { return fa->s[1]==this; }
		node* link(const int w,node *t) { s[w]=t; if(t)t->fa=this; return this; }
		int	Gcd(int a,int b)
		{ if(a<b)swap(a,b);if(b<=0)return a>0?a:0;int c=a%b;while(c)a=b,b=c,c=a%b;return b; }
		void	update()
		{
			size=1+(s[0]?s[0]->size:0)+(s[1]?s[1]->size:0);
			gcd[0]=Gcd(s[0]?s[0]->gcd[0]:0,s[1]?s[1]->gcd[0]:0);
			gcd[1]=Gcd(s[0]?s[0]->gcd[1]:0,s[1]?s[1]->gcd[1]:0);
			gcd[st]=Gcd(gcd[st],val);
		}
	}*root,U[310000],*Trash[310000];int ALL,top;
	node* ALLOC(const int x,const int y)
	{ node *nd=top?Trash[top--]:new(U+(++ALL))node; *nd=node(x,y); return nd; }
	void	RECYCLE(node * t) { Trash[++top]=t; }
	void	unlink(node * t,const int w) { RECYCLE(t->s[w]);t->s[w]=0; return ; }

	Splay() { clear(); }

	void	clear()
	{
		ALL=top=0;root=ALLOC(-1,0);
		node *nd=ALLOC(-2,0);
		root->link(1,nd);
		root->update();
	}

	void	rot(node *t)
	{
		node *gfa=t->fa->fa;
		t->getlr()?t->link(0,t->fa->link(1,t->s[0])):
			t->link(1,t->fa->link(0,t->s[1]));
		t->fa->update();
		if(gfa)gfa->link(gfa->s[1]==t->fa,t);
		else	t->fa=0,root=t;
	}

	void	splay(node *t,node *tar)
	{
		while(t->fa!=tar && t->fa->fa!=tar)
			t->getlr()==t->fa->getlr()?
				(rot(t->fa),rot(t)):(rot(t),rot(t));
		if(t->fa!=tar)rot(t);
		t->update();
	}

	node* find(int k)
	{
		node *t=root;
		int w=t->s[0]?t->s[0]->size:0;
		while(w+1!=k)
		{
			if(w>=k)t=t->s[0];
			else k-=w+1,t=t->s[1];
			w=t->s[0]?t->s[0]->size:0;
		}
		return t;
	}

	void	insert(const int pos,const int d,const int dd)
	{ split(pos,pos-1); root->s[1]->link(0,ALLOC(d,dd)); }

	void split(const int l,const int r)
	{ splay(find(l),0);splay(find(r+2),root); }

	int	query(const int l,const int r,const int st)
	{ split(l,r); if(root->s[1]->s[0]->gcd[st]==0)return -1;return root->s[1]->s[0]->gcd[st]; }

	void	erase(const int pos)
	{ split(pos,pos); unlink(root->s[1],0); root->s[1]->update(); root->update(); }

	void	flip(const int pos)
	{ split(pos,pos); root->s[1]->s[0]->st^=1; root->s[1]->s[0]->update(); root->s[1]->update(); root->update(); }

	void	change(const int pos,const int x)
	{ split(pos,pos); root->s[1]->s[0]->val=x;
		root->s[1]->s[0]->update(); root->s[1]->update(); root->update();}
}S;

int	n,m;

int main()
{
	int	x,y,z,i;
	char	op[10];

	while(scanf("%d%d",&n,&m)==2)
	{
		S.clear();
		for(i=1;i<=n;++i)
		{
			scanf("%d%d",&x,&y);
			S.insert(i,x,y);
		}

		for(i=1;i<=m;++i)
		{
			scanf("%s",op);
			if(op[0]=='Q')
			{
				scanf("%d%d%d",&x,&y,&z);
				printf("%d\n",S.query(x,y,z));
			}
			if(op[0]=='I')
			{
				scanf("%d%d%d",&x,&y,&z);
				S.insert(x+1,y,z);
			}
			if(op[0]=='D')
			{
				scanf("%d",&x);
				S.erase(x);
			}
			if(op[0]=='R')
			{
				scanf("%d",&x);
				S.flip(x);
			}
			if(op[0]=='M')
			{
				scanf("%d%d",&x,&y);
				S.change(x,y);
			}
		}
	}
	return 0;
}
