#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <ctime>

using namespace std;

struct node { node*	ch[27]; int	Sum; node(){memset(ch,0,sizeof(ch)),Sum=0;}}*root;

int	n;

void	Insert(node*& t,const char* val)
{
	if(!t)t=new node;
	t->Sum++;
	if(!*val)return ;
	Insert(t->ch[val[0]-'a'],val+1);
	return ;
}

int	Find(node*& t,const char* val)
{
	if(!t)return 0;
	if(!*val)return t->Sum;
	return Find(t->ch[val[0]-'a'],val+1);
}

char	getstr(char* str)
{
	char	ch=getchar();
	if(ch=='\n')return 0;
	int	c=0;
	while((ch>0 && ch<'A') || (ch>'Z' && ch<'a') || ch>'z'){printf("%c",ch),ch=getchar();if(ch=='\n')return ch;}
	while((ch>='a' && ch<='z') || (ch>='A' && ch<='Z')) { str[c++]=ch; ch=getchar(); }
	str[c]=0;
	return ch;
}

char	str[20];

int main()
{
	root=new node;
	while(getstr(str)) Insert(root,str);
	while(~getstr(str)) printf("%d\n",Find(root,str));
	return 0;
}
