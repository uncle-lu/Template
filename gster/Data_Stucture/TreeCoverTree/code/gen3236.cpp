#include <bits/stdc++.h>

using namespace std;

int main(int argc,char ** argv)
{
	assert(argc>=3);
	int	n=atoi(argv[1]),m=atoi(argv[2]);
	random_device	seed;
	mt19937		RND(seed());
	cout << n << ' ' << m << endl;
	for(int i=1;i<=n;++i)
		cout << RND()%(n/2)+1 << ' ';
	cout << endl;
	for(int i=1;i<=m;++i)
	{
		int	x=RND()%n+1,y=RND()%n+1;
		int	a=RND()%(n/2)+1,b=RND()%(n/2)+1;
		if(x>y)swap(x,y);
		if(a>b)swap(a,b);
		cout << x << ' ' << y << ' ' << a << ' ' << b << endl;
	}
	return 0;
}
