#include <bits/stdc++.h>

using namespace std;

int	a[110000];

int main()
{
	int	n,m,i;
	
	scanf("%d%d",&n,&m);
	for(i=1;i<=n;++i)
		scanf("%d",&a[i]);
	while(m--)
	{
		int	x,y,l,r;
		scanf("%d%d%d%d",&x,&y,&l,&r);
		int	cnt=0;
		set<int>S;
		for(i=x;i<=y;++i)
			if(a[i]>=l && a[i]<=r)cnt++,S.insert(a[i]);
		printf("%d %d\n",cnt,(int)S.size());
	}
	return 0;
}

