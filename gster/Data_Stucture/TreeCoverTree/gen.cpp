#include <bits/stdc++.h>

using namespace std;

int main(int argc,char ** argv)
{
	assert(argc>=3);
	int	n=atoi(argv[1]),m=atoi(argv[2]);
	random_device	seed;
	mt19937		RND(seed());
	cout << n << endl;
	for(int i=1;i<=n;++i) { cout << RND()%100 << ' '; }
	cout << endl << m << endl;
	while(m--)
	{
		int	x=RND()%n+1,y=RND()%n+1;
		if(x>y)swap(x,y);
		cout << x << ' ' << y << endl;
	}
	return 0;
}
