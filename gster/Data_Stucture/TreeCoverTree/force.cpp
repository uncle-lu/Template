#include <bits/stdc++.h>
#pragma	GCC	optimize(2)

using namespace std;

int	a[110000],n,m;

int	Calc()
{
	int	cnt=0;
	for(int i=2;i<=n;++i)
		for(int j=1;j<=i;++j)
			if(a[i]<a[j])cnt++;
	return cnt;
}

int main()
{
	scanf("%d\n",&n);
	for(int i=1;i<=n;++i) scanf("%d",&a[i]);
	printf("%d\n",Calc());
	scanf("%d",&m);
	while(m--)
	{
		int	x,y;
		scanf("%d%d",&x,&y);
		swap(a[x],a[y]);
		printf("%d\n",Calc());
	}
	return 0;
}
