#include<iostream>
#include<cstdlib>
#include<algorithm>
#include<cstring>
#define ll long long 
using namespace std;
int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int a,b,c;
int n,m,sz;
int root[200005];
int ls[20000005],rs[20000005],sum[20000005],lazy[20000005];
void pushdown(int k,int l,int r)
{
	if(!lazy[k]||l==r)return;
	if(!ls[k])ls[k]=++sz;
	if(!rs[k])rs[k]=++sz;
	lazy[ls[k]]+=lazy[k];lazy[rs[k]]+=lazy[k];
	int mid=(l+r)>>1;
	sum[ls[k]]+=(mid-l+1)*lazy[k];
	sum[rs[k]]+=(r-mid)*lazy[k];
	lazy[k]=0;
}
void modify(int &k,int l,int r,int a,int b)
{
	if(!k)k=++sz;
	pushdown(k,l,r);
	if(l==a&&r==b)
	{
		sum[k]+=r-l+1;
		lazy[k]++;
		return;
	}
	int mid=(l+r)>>1;
	if(b<=mid)modify(ls[k],l,mid,a,b);
	else if(a>mid)modify(rs[k],mid+1,r,a,b);
	else 
	{
		modify(ls[k],l,mid,a,mid);modify(rs[k],mid+1,r,mid+1,b);
	}
	sum[k]=sum[ls[k]]+sum[rs[k]];
}
int query(int k,int l,int r,int a,int b)
{
	if(!k)return 0;
	pushdown(k,l,r);
	if(l==a&&r==b)return sum[k];
	int mid=(l+r)>>1;
	if(b<=mid)return query(ls[k],l,mid,a,b);
	else if(a>mid)return query(rs[k],mid+1,r,a,b);
	else return query(ls[k],l,mid,a,mid)+query(rs[k],mid+1,r,mid+1,b);
}
void insert()
{
	int k=1,l=1,r=n;
	while(l!=r)
	{
		int mid=(l+r)>>1;
		modify(root[k],1,n,a,b);
		if(c<=mid)r=mid,k=k<<1;
		else l=mid+1,k=k<<1|1;
	}
	modify(root[k],1,n,a,b);
}
int solve()
{
	int l=1,r=n,k=1;
	while(l!=r)
	{
		int mid=(l+r)>>1;
		int t=query(root[k<<1],1,n,a,b);
		if(t>=c)r=mid,k<<=1;
		else l=mid+1,k=k<<1|1,c-=t;
	}
	return l;
}
int main()
{
	n=read();m=read();
	while(m--)
	{
		int f=read();a=read();b=read();c=read();
		if(f==1)
		{
			c=n-c+1;insert();
		}
		else printf("%d\n",n-solve()+1);
	}
	return 0;
}
