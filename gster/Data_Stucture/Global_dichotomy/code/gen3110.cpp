#include <bits/stdc++.h>

using namespace std;

int	Sum[270000],f[270000];

void	push_down(const int num,const int l,const int r)
{
	if(f[num])
	{ if(l!=r) { f[num<<1]+=f[num]; f[num<<1|1]+=f[num]; }
		Sum[num]+=f[num]*(r-l+1); f[num]=0; } return ;
}

void	update(const int num,const int l,const int r)
{
	int	mid=l+((r-l)>>1);
	push_down(num<<1,l,mid);
	push_down(num<<1|1,mid+1,r);
	Sum[num]=Sum[num<<1]+Sum[num<<1|1];
	return ;
}

void	Add(const int l,const int r,const int num,const int s,const int t)
{
	if(s<=l && r<=t) { f[num]++; push_down(num,l,r); return ; }
	int	mid=l+((r-l)>>1); push_down(num,l,r);
	if(s<=mid)Add(l,mid,num<<1,s,t);
	if(t>mid) Add(mid+1,r,num<<1|1,s,t);
	update(num,l,r);
	return ;
}

int	Sumup(const int l,const int r,const int num,const int s,const int t)
{
	if(s<=l && r<=t) { push_down(num,l,r); return Sum[num]; }
	int	mid=l+((r-l)>>1); push_down(num,l,r);
	if(t<=mid)return Sumup(l,mid,num<<1,s,t);
	if(s>mid) return Sumup(mid+1,r,num<<1|1,s,t);
	return Sumup(l,mid,num<<1,s,t)+Sumup(mid+1,r,num<<1|1,s,t);
}

int main(int argc,char ** argv)
{
	assert(argc>=3);
	int	n=atoi(argv[1]),m=atoi(argv[2]),T;
	random_device	seed;
	mt19937		RND((T=seed()));
	cerr << "Random Seed ID: " << 1U*T << endl;
	cout << n << ' ' << m << endl;
	while(m--)
	{
		int op=RND()%3;
		if(op==0)
		{
ADD:
			int	x=RND()%n+1,y=RND()%n+1;
			if(x>y)swap(x,y); Add(1,n,1,x,y);
			cout << 1 << ' ' << x << ' ' << y << ' ' << (RND()%2?1:-1)*((int)RND()%n+1) << endl;
			continue;
		}
		else
		{
			int	x=RND()%n+1,y=RND()%n+1;
			if(x>y)swap(x,y);
			if(Sumup(1,n,1,x,y)==0)goto ADD;
			cout << 2 << ' ' << x << ' ' << y << ' ' << RND()%Sumup(1,n,1,x,y)+1 << endl;
			continue;
		}
	}
	return 0;
}
