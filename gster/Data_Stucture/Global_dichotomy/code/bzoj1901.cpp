#include <bits/stdc++.h>
#pragma	GCC	optimize(3)

using namespace std;

struct qr
{
	int	tp,l,r,k,Ans; qr(){}
	qr(const int ar1,const int ar2,const int ar3,const int ar4):
	tp(ar1),l(ar2),r(ar3),k(ar4){}
}Q[51000];

int	n,m,a[11000],cnt;
int	pos[51000],pp[51000],S[51000];

void	Add(int i,const int d) { for (;i<=cnt;i+=i&-i)S[i]+=d; }
int	Query(int i) { int temp=0; for (;i;i-=i&-i)temp+=S[i]; return temp; }
int	Query(const int l,const int r) { return Query(r)-Query(l-1); }

void	Solve(const int l,const int r,const int L,const int R)
{
	if(l>r)return ;
	if (L==R)
	{
		for (int i=l;i<=r;++i)
			if (Q[pos[i]].tp==2) Q[pos[i]].Ans=L;
		return ;
	}

	int	mid=L+((R-L)>>1),ll=l-1,rr=r+1;
	for (int i=l;i<=r;++i)
	{
		if (Q[pos[i]].tp==1)
		{
			if (Q[pos[i]].r<=mid)
				Add(Q[pos[i]].l,Q[pos[i]].k),pp[++ll]=pos[i];
			else pp[--rr]=pos[i];
		}
		else
		{
			int	temp=Query(Q[pos[i]].l,Q[pos[i]].r);
			if(temp>=Q[pos[i]].k)pp[++ll]=pos[i];
			else	Q[pos[i]].k-=temp,pp[--rr]=pos[i];
		}
	}

	for (int i=l;i<=r;++i)
	{
		if (Q[pos[i]].tp==1 && Q[pos[i]].r<=mid)
			Add(Q[pos[i]].l,-Q[pos[i]].k);
		if(i<=ll)pos[i]=pp[i];
		else pos[i]=pp[r-i+rr];
	}

	Solve(l,ll,L,mid);
	Solve(rr,r,mid+1,R);
	return ;
}

int	getint()
{
	int	data=0;char ch=getchar_unlocked();
	while(ch<'0' || ch>'9')ch=getchar_unlocked();
	while(ch>='0' && ch<='9')data=data*10+ch-48,ch=getchar_unlocked();
	return data;
}

int main()
{
	int	i,x,y,z;
	char	op[10];

	n=getint();m=getint();
	for (i=1;i<=n;++i)
	{
		a[i]=getint();
		Q[++cnt]=qr(1,i,a[i],1);
	}

	for (i=1;i<=m;++i)
	{
		scanf("%s",op);
		if (op[0]=='C')
		{
			x=getint(),y=getint();
			Q[++cnt]=qr(1,x,a[x],-1);
			a[x]=y;
			Q[++cnt]=qr(1,x,a[x],1);
		}
		else
		{
			x=getint(),y=getint(),z=getint();
			Q[++cnt]=qr(2,x,y,z);
		}
	}

	for (i=1;i<=cnt;++i) pos[i]=i;

	Solve(1,cnt,-1,1e9+1);

	for (i=1;i<=cnt;++i)
		if(Q[i].tp==2)printf("%d\n",Q[i].Ans);
	
	return 0;
}
