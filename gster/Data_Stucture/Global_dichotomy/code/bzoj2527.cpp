#include <bits/stdc++.h>

using namespace std;

struct node{int x,y,z;}p[510000];
int n,m,c[510000],id[510000],ans[510000];
int g[510000],to[510000],nxt[510000],tot;
int tol[510000],tor[510000];
long long h[510000],tmp[510000],cur[510000];

void add(const int  pos,const int  x) { for(int i=pos;i<=m;i+=i&-i)h[i]+=x; }
void adddt(const int x,const int y,const int z) { add(x,z); add(y+1,-z); }
long long sum(const int pos) { long long t=0; for(int i=pos;i;i-=i&-i) t+=h[i]; return t; }
void addop(const int x,const int y,const int z,const int i) { p[i].x=x; p[i].y=y; p[i].z=z; }
void addpt(const int x,const int y) { to[++tot]=y; nxt[tot]=g[x]; g[x]=tot; }
void solve(const int head,const int tail,const int l,const int r)
{
	if (head>tail) return;
	int i,k,mid=(l+r)>>1,lnum=0,rnum=0;
	if(l==r)
	{
		for(i=head;i<=tail;i++) ans[id[i]]=l;
		return;
	}
	for(i=l;i<=mid;i++)
	{
		if (p[i].x<=p[i].y) adddt(p[i].x,p[i].y,p[i].z);
		else adddt(p[i].x,m,p[i].z),adddt(1,p[i].y,p[i].z);
	}
	for(i=head;i<=tail;i++)
	{
		tmp[id[i]]=0;
		for(k=g[id[i]];k;k=nxt[k])
		{
			tmp[id[i]]+=sum(to[k]);
			if (tmp[id[i]]+cur[id[i]]>c[id[i]]) break;
		}
		if (cur[id[i]]+tmp[id[i]]>=c[id[i]]) tol[++lnum]=id[i];
		else tor[++rnum]=id[i],cur[id[i]]+=tmp[id[i]];
	}
	for(i=l;i<=mid;i++)
	{
		if(p[i].x<=p[i].y) adddt(p[i].x,p[i].y,-p[i].z);
		else adddt(p[i].x,m,-p[i].z),adddt(1,p[i].y,-p[i].z);
	}
	for(i=0;i<lnum;i++) id[head+i]=tol[i+1];
	for(i=0;i<rnum;i++) id[head+lnum+i]=tor[i+1];
	solve(head,head+lnum-1,l,mid);
	solve(head+lnum,tail,mid+1,r);
}

int main()
{
	int i,x,y,z,k;
	scanf("%d%d",&n,&m);
	for(i=1;i<=m;i++)
	{
		scanf("%d",&x);
		addpt(x,i);
	}
	for(i=1;i<=n;i++)
	{
		scanf("%d",&c[i]);
		id[i]=i;
	}
	scanf("%d",&k);
	for(i=1;i<=k;i++)
	{
		scanf("%d%d%d",&x,&y,&z);
		addop(x,y,z,i);
	}
	addop(1,m,0x3f3f3f3f,++k);
	solve(1,n,1,k);
	for(i=1;i<=n;i++) if(ans[i]!=k) printf("%d\n",ans[i]);
	else printf("NIE\n");
	return 0;
}
