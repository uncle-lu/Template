#include <bits/stdc++.h>
#pragma	GCC	optimize(3)

using namespace std;

int	n,m,f[150000],Sum[150000],pos[51000],pp[51000];

struct query { int	l,r,tp,Ans; unsigned int k; }q[51000];

void	push_down(const int num,const int l,const int r)
{
	if(f[num])
	{ if(l!=r) { f[num<<1]+=f[num]; f[num<<1|1]+=f[num]; }
		Sum[num]+=f[num]*(r-l+1); f[num]=0; } return ;
}

void	update(const int num,const int l,const int r)
{
	int	mid=l+((r-l)>>1);
	push_down(num<<1,l,mid);
	push_down(num<<1|1,mid+1,r);
	Sum[num]=Sum[num<<1]+Sum[num<<1|1];
	return ;
}

void	Add(const int l,const int r,const int num,const int s,const int t,const int d)
{
	if(s<=l && r<=t) { f[num]+=d; push_down(num,l,r); return ; }
	int	mid=l+((r-l)>>1); push_down(num,l,r);
	if(s<=mid)Add(l,mid,num<<1,s,t,d);
	if(t>mid) Add(mid+1,r,num<<1|1,s,t,d);
	update(num,l,r);
	return ;
}

int	Query(const int l,const int r,const int num,const int s,const int t)
{
	if(s<=l && r<=t) { push_down(num,l,r); return Sum[num]; }
	int	mid=l+((r-l)>>1); push_down(num,l,r);
	if(t<=mid)return Query(l,mid,num<<1,s,t);
	if(s>mid) return Query(mid+1,r,num<<1|1,s,t);
	return Query(l,mid,num<<1,s,t)+Query(mid+1,r,num<<1|1,s,t);
}

void	Solve(const int l,const int r,const int L,const int R)
{
	if(l>r)return ;
	if(L==R)
	{
		for(int i=l;i<=r;++i)q[pos[i]].Ans=L;
		return ;
	}
	int	mid=L+((R-L)>>1),ll=l-1,rr=r+1;
	for(int i=l;i<=r;++i)
	{
		if(q[pos[i]].tp==1)
		{
			if(q[pos[i]].k>1U*mid)Add(1,n,1,q[pos[i]].l,q[pos[i]].r,1);
			if(q[pos[i]].k>1U*mid)pp[--rr]=pos[i];
			else		pp[++ll]=pos[i];
		}
		if(q[pos[i]].tp==2)
		{
			int	temp=Query(1,n,1,q[pos[i]].l,q[pos[i]].r);
			if(1U*temp>=q[pos[i]].k)pp[--rr]=pos[i];
			else	   pp[++ll]=pos[i],q[pos[i]].k-=temp;
		}
	}
	for(int i=l;i<=r;++i)
		if(q[pos[i]].tp==1 && q[pos[i]].k>1U*mid)Add(1,n,1,q[pos[i]].l,q[pos[i]].r,-1);
	for(int i=l;i<=ll;++i)pos[i]=pp[i];
	for(int i=r;i>=rr;--i)pos[ll+1+r-i]=pp[i];
	Solve(rr,r,mid+1,R);
	Solve(l,ll,L,mid);
	return ;
}

int main()
{
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;++i)
	{
		unsigned int temp;
		scanf("%d%d%d%u",&q[i].tp,&q[i].l,&q[i].r,&temp);
		if(q[i].tp==1)q[i].k=temp+n; else q[i].k=temp; pos[i]=i;
	}
	Solve(1,m,0,n<<1|1);
	for(int i=1;i<=m;++i)
		if(q[i].tp==2)printf("%d\n",q[i].Ans?q[i].Ans-n:1);
	return 0;
}
