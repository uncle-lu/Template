#include <bits/stdc++.h>

using namespace std;

template<const int _n,const int _m>
struct Edge
{
	struct Edge_base
	{
		int to,next;
	} e[_m];
	int cnt,p[_n];
	Edge()
	{
		clear();
	} void clear()
	{
		cnt=0,memset(p,0,sizeof(p));
	}
	void	insert(const int x,const int y)
	{
		e[++cnt].to=y;
		e[cnt].next=p[x];
		p[x]=cnt;
		return ;
	}
	int	start(const int x)
	{
		return p[x];
	}
	Edge_base& operator[](const int x)
	{
		return e[x];
	}
};

int	n,m,s[310000][2],fa[31000];
int	Max[310000],Sum[310000],val[310000];
Edge<310000,610000>e;

void	update(const int x)
{
	if(!x)return ;
	Max[x]=max(val[x],max(Max[s[x][0]],Max[s[x][1]]));
	Sum[x]=Sum[s[x][0]]+Sum[s[x][1]]+val[x];
}

bool	isroot(const int x)
{
	return s[fa[x]][0]!=x && s[fa[x]][1]!=x;
}

void	link(const int x,const int y,const int c)
{
	fa[x]=y;
	s[y][c]=x;
	return ;
}

void	rotate(const int x,const int c)
{
	int	y=fa[x];
	if(!isroot(y)) link(x,fa[y],s[fa[y]][1]==y);
	else fa[x]=fa[y];
	link(s[x][!c],y,c);
	link(y,x,!c);
	update(y);
}

void	splay(int x)
{
	while(!isroot(x))
	{
		int y=fa[x];
		int cy=(s[fa[y]][1]==y),cx=(s[y][1]==x);
		if(isroot(y))rotate(x,cx);
		else
		{
			if(cx==cy)rotate(y,cy);
			else rotate(x,cx);
			rotate(x,cy);
		}
	}
	update(x);
	return ;
}

int	Access(int x)
{
	int y;
	for(y=0; x; y=x,x=fa[x])
	{
		splay(x);
		s[x][1]=y;
		update(x);
	}
	return y;
}

int	Query_Max(const int x,const int y)
{
	Access(x);
	int	lca=Access(y);
	splay(x);
	if(lca==x)return max(val[lca],Max[s[lca][1]]);
	return max(val[lca],max(Max[s[lca][1]],Max[x]));
}

int	Query_Sum(const int x,const int y)
{
	Access(x);
	int	lca=Access(y);
	splay(x);
	if(lca==x)return val[lca]+Sum[s[lca][1]];
	return val[lca]+Sum[s[lca][1]]+Sum[x];
}

void	Change(const int x,const int y)
{
	splay(x);
	val[x]=y;
	update(x);
}

void	Dfs(const int S,const int Fa)
{
	fa[S]=Fa;
	for(int i=e.start(S); i; i=e[i].next)
	{
		if(e[i].to==Fa)continue;
		Dfs(e[i].to,S);
	}
	return ;
}

int main()
{
	int	i,x,y;
	char	op[10];
	scanf("%d",&n);
	memset(Max,0x80,sizeof(Max));
	for(i=1; i<n; ++i) scanf("%d%d",&x,&y),e.insert(x,y),e.insert(y,x);
	for(i=1; i<=n; ++i) scanf("%d",&val[i]);
	Dfs(1,0);
	scanf("%d",&m);
	while(m--)
	{
		scanf("%s%d%d",op,&x,&y);
		if(op[1]=='S') printf("%d\n",Query_Sum(x,y));
		if(op[1]=='M') printf("%d\n",Query_Max(x,y));
		if(op[1]=='H') Change(x,y);
	}

	return 0;
}
