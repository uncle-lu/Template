#include <bits/stdc++.h>

using namespace std;

int main(int argc,char ** argv)
{
	if(argc<3)return 0;
	random_device	seed;
	mt19937		RND(seed());
	int	n=atoi(argv[1]),m=atoi(argv[2]),lim=20,i;
	vector<int>	vec;
	if(argc==4)lim=atoi(argv[3]);

	cout << n << endl;
	for(i=1;i<=n;++i)vec.push_back(i);
	shuffle(vec.begin(),vec.end(),mt19937(seed()));
	for(i=1;i<(int)vec.size();++i)
	{
		cout << vec[RND()%i] << ' ' << vec[i] << endl;
	}

	for(i=1;i<=n;++i)
	{
		cout << (int)RND()%lim << ' ';
	}cout << endl;

	cout << m << endl;
	for(i=1;i<=m;++i)
	{
		int	op=RND()%100;
		if(i==m)op=RND()%40;
		if(op<20)
		{
			int	x=RND()%n+1,y=RND()%n+1;
			if(x>y)swap(x,y);
			cout << "QSUM " << x << ' ' << y << endl;
		}
		else if(op<40)
		{
			int	x=RND()%n+1,y=RND()%n+1;
			if(x>y)swap(x,y);
			cout << "QMAX " << x << ' ' << y << endl;
		}
		else
		{
			int	x=RND()%n+1,y=(int)RND()%lim;
			cout << "CHANGE " << x << ' ' << y << endl;
		}
	}
	return 0;
}
