#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <ctime>
#include <queue>
#include <stack>

using namespace std;

struct get_time
{
	stack<int>	st;
	int	Sum,cnt;
	void set()
	{
		st.push(clock());cnt++;
	}
	int	get()
	{
		int t=clock()-st.top();
		Sum+=t;
		st.pop();
		return t;
	}
}F[100];


template<const int _n,const int BLOCK_SIZE>
struct Block_List
{
	struct Block
	{
		char	val[BLOCK_SIZE+3];
		int	size;
		Block*	next;
		Block() { clear(); }
		void	clear() { size=0,memset(val,0,sizeof(val)); }
	}*begin,*end,U[3100];
	int	ALL;
	queue<Block*>	Trash;

	inline Block*	ALLOC()
	{
		F[0].set();
		if(!Trash.empty())
		{
			Block* t=Trash.front();
			Trash.pop();
			return t;
		}
		F[0].get();
		return U+(ALL++);
	}

	inline void	RECYCLE(Block*& t)
	{
		F[1].set();
		Trash.push(t);
		t=0;
		F[1].get();
	}

	Block_List()
	{
		F[2].set();
		ALL=0;
		begin=ALLOC();
		end=ALLOC();
		begin->next=end;
		F[2].get();
	}

	pair<Block*,int>	Get_Block(const int pos)
	{
		F[3].set();
		int	Sum=0;
		Block*	t;
		for(Block* i=begin;i!=end;i=i->next)
		{
			t=i;
			Sum+=i->size;
			if(Sum>pos)return make_pair(i,pos-(Sum-i->size));
		}
		F[3].get();
		return make_pair(t,t->size);
	}

	void	split(Block*& t,const int pos)
	{
		F[4].set();
		Block*	nd=ALLOC();
		for(int i=pos;i<t->size;++i)
		{
			nd->val[i-(pos)]=t->val[i];
			t->val[i]=0;
		}
		nd->size=t->size-(pos);
		t->size=(pos);
		nd->next=t->next;
		t->next=nd;
		F[4].get();
	}

	inline void	Union(Block*& t1,Block*& t2)
	{
		F[5].set();
		for(int i=0;i<t2->size;++i)
			f_push_back(t1,t2->val[i]),t2->val[i]=0;
		Block*	temp=t2->next;
		RECYCLE(t2);
		t1->next=temp;
		F[5].get();
	}

	inline void	Reunion()
	{
		F[6].set();
		for(Block* i=begin;i!=end;i=i->next)
		{
			if(i->next!=end && i->size+i->next->size<=BLOCK_SIZE)
				Union(i,i->next);
		}
		F[6].get();
	}
	
	inline void	f_push_back(Block*& t,const int d)
	{
		F[7].set();
		t->val[t->size]=d;
		t->size++;
		F[7].get();

	}

	inline void	push_back(Block*& t,const int d)
	{
		F[8].set();
		if(t->size>=BLOCK_SIZE)
		{
			split(t,BLOCK_SIZE>>1);
			push_back(t->next,d);
		}
		else
		{
			t->val[t->size]=d;
			t->size++;
		}
		F[8].get();
	}

	inline void	Insert(Block*& t,const int pos,const int d)
	{
		F[9].set();
		if(pos==t->size) { push_back(t,d); }
		else
		{
			split(t,pos);
			push_back(t,d);
		}
		F[9].get();
	}

	void	insert(const int pos,const int d)
	{
		F[10].set();
		pair<Block*,int>	t=Get_Block(pos);
		Insert(t.first,t.second,d);
		//Reunion();
		F[10].get();
	}

	void	set(const char* str,const int n)
	{
		F[13].set();
		Block*	t=begin;
		for(int i=0;i<n;++i)
		{
			if(i && i%BLOCK_SIZE==0)
			{
				Block* nd=ALLOC();
				t->next=nd;
				t=t->next;
			}
			t->val[i%BLOCK_SIZE]=str[i];t->size++;
		}
		t->next=end;
		F[13].get();
		return ;
	}

	char&	operator[](const int x)
	{
		F[11].set();
		pair<Block*,int>t=Get_Block(x);
		return t.first->val[t.second];
		F[11].get();
	}
};

int	n;
Block_List<1002005,1001>L;
char	s[1100000],op[20],ch[20];

int main()
{
	F[12].set();
	int	T,x;

	scanf("%s",s);
	n=strlen(s);
	L.set(s,n);
	scanf("%d",&T);
	while(T--)
	{
		scanf("%s",op);
		if(op[0]=='Q')
		{
			scanf("%d",&x);
			printf("%c\n",L[x-1]);
		}
		else
		{
			scanf("%s%d",ch,&x);
			L.insert(x-1,ch[0]);
		}
	}
	F[12].get();
	for(int i=0;i<=13;++i)
	{
		cerr << (double)F[i].Sum/CLOCKS_PER_SEC << '\t' << F[i].cnt << endl;
	}
	return 0;
}
