#include<cstdio>//BY:uncle-lu
using namespace std;
template<class T>void read(T &x)
{
	x=0;
	int f=0;
	char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch>='0'&&ch<='9') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int gcd(int a,int b) { return b ? gcd(b,a%b):a; }

int main()
{
	int a,b;
	read(a);
	read(b);
	int k=gcd(a,b);
	printf("%d",k);
	return 0;
}
