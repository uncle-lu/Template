/*
	Data_struct Heap
	BY:uncle-lu
	2017-08-04

	Help:
		init()为初始化
		insert()插入值
		remove()删除头顶元素
		top()返回堆顶元素
		create()堆化数组
		heap_sort()堆排序
*/
#include<algorithm>
using namespace std;
struct Heap
{
	int line[10000];
	int n;
	
	void init()
	{
		n=0;
		return ;
	}

	void insert(int x)
	{
		n++;
		line[n]=x;
		int k=n;
		for(int i=(n>>1);i;i>>=1)
		{
			if(line[i]<line[k])
			{
				swap(line[i],line[k]);
				k=i;
			}
			else break;
		}
		return ;
	}

	void pop_heap()
	{
		line[1]=line[n];
		n--;
		int k=1;
		int j=k<<1;
		while(k<n&&j<n)
		{
			if(j+1<n&&a[j]>a[j+1])j+=1;
			if(a[j]>a[k])break;
			swap(line[k],line[j]);
			swap(j,k);
			j=k<<1;
		}
		return ;
	}

	int top()
	{
		return line[1];
	}

};
