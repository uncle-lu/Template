#include<cstdio>//SA
#include<cstring>
#include<algorithm>
using namespace std;

const int maxn=100;
char s[maxn];
int rn[maxn];
int san[maxn];

int wa[maxn],wb[maxn],wv[maxn],ws[maxn];

int cmp(int *r,int a,int b,int l) { return r[a]==r[b]&&r[a+l]==r[b+l]; }

void da(int *r,int *sa,int n,int m)
{
	int i,p,j,*x=wa,*y=wb,*t;
	for(i=0;i<m;++i)
	{
		ws[i]=0;
	}
	for(i=0;i<n;++i)
	{
		ws[x[i]=r[i]]++;
	}
	for(i=1;i<m;++i)
	{
		ws[i]+=ws[i-1];
	}
	for(i=n-1;i>=0;--i)
	{
		sa[--ws[x[i]]]=i;
	}
	for(j=1,p=1;p<n;j<<=1,m=p)
	{
		for(p=0,i=n-j;i<n;i++)
		{
			y[p++]=i;
		}
		for(i=0;i<n;++i)
		{
			if(sa[i]>=j)y[p++]=sa[i]-j;
		}
		for(i=0;i<n;++i)
		{
			wv[i]=x[y[i]];
		}
		for(i=0;i<m;++i)
		{
			ws[i]=0;
		}
		for(i=0;i<n;++i)
		{
			ws[wv[i]]++;
		}
		for(i=1;i<m;++i)
		{
			ws[i]+=ws[i-1];
		}
		for(i=n-1;i>=0;--i)
		{
			sa[--ws[wv[i]]]=y[i];
		}
		for(t=x,x=y,y=t,p=1,x[sa[0]]=0,i=1;i<n;++i)
		{
			x[sa[i]]=cmp(y,sa[i-1],sa[i],j)?p-1:p++;
		}
	}
	return ;
}

int main()
{
	int max_val=0;
	scanf("%s",s);
	int l=strlen(s);
	for(int i=0;i<l;++i)
	{
		rn[i]=s[i]-'a'+1;
		max_val=max(max_val,rn[i]);
	}
	da(rn,san,l,max_val+1);
	for(int i=0;i<l;++i)rn[san[i]]=i;
	for(int i=0;i<l;++i)printf("%d ",rn[i]+1);
	return 0;
}
